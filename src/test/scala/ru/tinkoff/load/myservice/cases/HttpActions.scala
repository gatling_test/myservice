package ru.tinkoff.load.myservice.cases

import io.gatling.http.Predef._
import io.gatling.core.Predef._

object HttpActions {

  val getMainPage = http(requestName = "GET /get")
    .get("/get")
    .header("accept", "appliction/json")
    .check(status is 200)

}
